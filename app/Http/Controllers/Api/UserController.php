<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    private $rules = [
        'name' => 'required',
        'email' => 'required',
        'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
        'password_confirmation' => 'min:6'
    ];

    private $rulesUpdate = [
        'name' => 'required'
    ];

    private $rulesChangePass = [
        'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
        'password_confirmation' => 'min:6'
    ];

    private $rulesChangeEmail = [
        'email' => 'required_with:email_confirmation|same:email_confirmation',
    ];

    private function setData(&$user, $request)
    {
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->api_token = Str::random(60);
    }

    public function index()
    {
        return "MMS";
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->rules);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->getMessageBag()->toArray()], 500);
            } else {
                if (!$this->findEmail($request->email)) {
                    $user = new User();
                    $this->setData($user, $request);
                    $user->save();
                    return response()->json(["user" => $user], 201);
                } else {
                    return response()->json(["warnings" => "El correo electrónico ya existe"], 200);
                }
            }
        } catch (\Exception $e) {
            return response()->json(["errors" => $e->getMessage()], 500);
        }

    }

    public function prueba(Request $request)
    {
        return "MAMALA";
    }

    private function findEmail($email)
    {
        $find = User::where('email', $email)->first();
        if ($find) {
            return true;
        } else {
            return false;
        }
    }

    public function show($id)
    {
        //return "OKAS";
        return Auth::user();
    }

    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), $this->rulesUpdate);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->getMessageBag()->toArray()], 500);
            } else {
                $user = User::find($id);
                $user->name = $request->name;
                $user->save();
                return response()->json(["user" => $user], 200);
            }
        } catch (\Exception $e) {
            return response()->json(["errors" => $e->getMessage()], 500);
        }
    }

    public function changePass(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), $this->rulesChangePass);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->getMessageBag()->toArray()], 500);
            } else {
                $user = User::find($id);
                $user->password = $request->password;
                $user->save();
                return response()->json(["user" => $user], 200);
            }
        } catch (\Exception $e) {
            return response()->json(["errors" => $e->getMessage()], 500);
        }
    }

    public function changeEmail(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), $this->rulesChangeEmail);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->getMessageBag()->toArray()], 500);
            } else {
                $user = User::find($id);
                $user->email = $request->email;
                $user->save();
                return response()->json(["user" => $user], 200);
            }
        } catch (\Exception $e) {
            return response()->json(["errors" => $e->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        //
    }
}
