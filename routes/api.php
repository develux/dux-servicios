<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*Route::middleware('auth:api')->get('/user', function(Request $request) {
    return $request->user();
});*/

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/prueba', "\App\Http\Controllers\Api\UserController@prueba");
    Route::get('/users/{id}', "\App\Http\Controllers\Api\UserController@show");
    Route::put('/users/{id}', "\App\Http\Controllers\Api\UserController@update");
    Route::delete('/users/{id}', "\App\Http\Controllers\Api\UserController@destroy");
});

Route::post('/users', "\App\Http\Controllers\Api\UserController@store");
